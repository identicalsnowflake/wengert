#ifndef WENGERT_T_H
#define WENGERT_T_H

#include <stdint.h>

typedef struct TapeOp TapeOp;

extern uint64_t run_tape(void* t , void* b1 , void* b2);
extern inline uint64_t sin_addr();
extern inline uint64_t cos_addr();
extern inline uint64_t tan_addr();
extern inline uint64_t asin_addr();
extern inline uint64_t acos_addr();
extern inline uint64_t atan_addr();
extern inline uint64_t sinh_addr();
extern inline uint64_t cosh_addr();
extern inline uint64_t tanh_addr();
extern inline uint64_t asinh_addr();
extern inline uint64_t acosh_addr();
extern inline uint64_t atanh_addr();
extern inline uint64_t atan2_addr();
extern inline uint64_t exp_addr();
extern inline uint64_t log_addr();

#endif
