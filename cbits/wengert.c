#include "wengert.h"
#include <stdio.h>
#include <math.h>


extern uint64_t run_tape(void* t , void* b1 , void* b2) {
  void* t0 = t + 8; // skip the counter
  void (*f)(void*,void*) = (void (*)(void*,void*))t0;
  f(b1,b2);
  return 0;
}

extern inline uint64_t sin_addr() {
  return (uint64_t)&sin;
}

extern inline uint64_t cos_addr() {
  return (uint64_t)&cos;
}

extern inline uint64_t tan_addr() {
  return (uint64_t)&tan;
}

extern inline uint64_t asin_addr() {
  return (uint64_t)&asin;
}

extern inline uint64_t acos_addr() {
  return (uint64_t)&acos;
}

extern inline uint64_t atan_addr() {
  return (uint64_t)&atan;
}

extern inline uint64_t sinh_addr() {
  return (uint64_t)&sinh;
}

extern inline uint64_t cosh_addr() {
  return (uint64_t)&cosh;
}

extern inline uint64_t tanh_addr() {
  return (uint64_t)&tanh;
}

extern inline uint64_t asinh_addr() {
  return (uint64_t)&asinh;
}

extern inline uint64_t acosh_addr() {
  return (uint64_t)&acosh;
}

extern inline uint64_t atanh_addr() {
  return (uint64_t)&atanh;
}

extern inline uint64_t atan2_addr() {
  return (uint64_t)&atan2;
}

extern inline uint64_t exp_addr() {
  return (uint64_t)&exp;
}

extern inline uint64_t log_addr() {
  return (uint64_t)&log;
}

