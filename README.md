# Wengert

Wengert is a library for automatic differentiation and gradient descent. The key design premise is to reify out to a flat tape of primops which is re-used across the steps of the descent, rather than re-computed each time as would be required with, say, the ad library.

```haskell
module Main where

import Wengert


data Two a = Two a a
  deriving (Show,Functor,Foldable,Traversable)

-- bowl shape in 3D space
bowl :: Num a => a -> a -> a
bowl x y = x ^ (2 :: Int)
         + y ^ (2 :: Int)

main :: IO ()
main = do

  let f :: Two (Var s Double) -> Var s Double
      f (Two x y) = bowl x y

      loc :: Two Double
      loc = Two 3 5
  
  -- the partials at (3,5) are:
  print $ grad f loc

  -- use gradient descent to find a local minima, staarting from our (3,5) location:
  print $ case advanceN 10 (\x -> -1 * f x) 0.2 loc of
    (_ , dest , _) -> dest


```

