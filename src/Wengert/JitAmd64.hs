module Wengert.JitAmd64 where

import Control.Monad.ST
import GHC.Exts
import GHC.Word
import qualified Data.Bits as B

import Wengert.CBindings


type Reg = Word8

arg0 :: Reg
arg0 = 7 -- rdi

arg1 :: Reg
arg1 = 6 -- rsi

resultReg :: Reg
resultReg = 0 -- rax

-- movsd reg0, [ reg1 + off ]
{-# INLINE load64 #-}
load64 :: ASMTape s -> Reg -> Reg -> Int -> ST s ()
load64 t r0 r1 off = do
  case 0b10000000 B..|. regMask0 r0 B..|. r1 of
    W8# w -> case 8 * off of
      I# i ->
        write64 t do
          B.unsafeShiftL (W64# (wordToWord64# (word8ToWord# w))) 24
            B..|. 0x0000000000100ff2
            B..|. B.unsafeShiftL (W64# (wordToWord64# (int2Word# i))) 32

{-# INLINE load1 #-}
-- load the constant 1.0 into a register. implemented as:
--   xor eax, eax
--   inc eax
--   cvtsi2sd r, eax
load1 :: ASMTape s -> Reg -> ST s ()
load1 t r = do
  case 0b11000000 B..|. regMask0 r of
    W8# w -> 
      write64 t do
        B.unsafeShiftL (W64# (wordToWord64# (word8ToWord# w))) 56
          B..|. 0x002a0ff2c0ffc031

{-# INLINE loadNeg1 #-}
-- load the constant -1.0 into a register. implemented as:
--   xor eax, eax
--   dec eax
--   cvtsi2sd r, eax
loadNeg1 :: ASMTape s -> Reg -> ST s ()
loadNeg1 t r = do
  case 0b11000000 B..|. regMask0 r of
    W8# w -> 
      write64 t do
        B.unsafeShiftL (W64# (wordToWord64# (word8ToWord# w))) 56
          B..|. 0x002a0ff2c8ffc031

-- movsd [ reg1 + off ], reg0
{-# INLINE store64 #-}
store64 :: ASMTape s -> Reg -> Reg -> Int -> ST s ()
store64 t r0 r1 off = do
  case 0b10000000 B..|. regMask0 r0 B..|. r1 of
    W8# w -> case 8 * off of
      I# i ->
        write64 t do
          B.unsafeShiftL (W64# (wordToWord64# (word8ToWord# w))) 24
            B..|. 0x0000000000110ff2
            B..|. B.unsafeShiftL (W64# (wordToWord64# (int2Word# i))) 32

{-# INLINE call #-}
-- mov rax, addr
-- call [ rax ]
call :: ASMTape s -> Word64 -> ST s ()
call t w = do
  write32 t 0xb8485657
  write64 t w
  write32 t 0xd0ff4890
  write16 t 0x5f5e

{-# INLINE add64 #-}
-- addsd r0, r1
add64 :: ASMTape s -> Reg -> Reg -> ST s ()
add64 t r0 r1 = do
  case 0b11000000 B..|. regMask0 r0 B..|. r1 of
    W8# w -> write32 t do
      B.unsafeShiftL (W32# (wordToWord32# (word8ToWord# w))) 24 B..|. 0x00580ff2

{-# INLINE sub64 #-}
-- subsd r0, r1
sub64 :: ASMTape s -> Reg -> Reg -> ST s ()
sub64 t r0 r1 = do
  case 0b11000000 B..|. regMask0 r0 B..|. r1 of
    W8# w -> write32 t do
      B.unsafeShiftL (W32# (wordToWord32# (word8ToWord# w))) 24 B..|. 0x005c0ff2

{-# INLINE mul64 #-}
-- mulsd r0, r1
mul64 :: ASMTape s -> Reg -> Reg -> ST s ()
mul64 t r0 r1 = do
  case 0b11000000 B..|. regMask0 r0 B..|. r1 of
    W8# w -> write32 t do
      B.unsafeShiftL (W32# (wordToWord32# (word8ToWord# w))) 24 B..|. 0x00590ff2

{-# INLINE div64 #-}
-- divsd r0, r1
div64 :: ASMTape s -> Reg -> Reg -> ST s ()
div64 t r0 r1 = do
  case 0b11000000 B..|. regMask0 r0 B..|. r1 of
    W8# w -> write32 t do
      B.unsafeShiftL (W32# (wordToWord32# (word8ToWord# w))) 24 B..|. 0x005e0ff2

{-# INLINE max64 #-}
-- maxsd r0, r1
max64 :: ASMTape s -> Reg -> Reg -> ST s ()
max64 t r0 r1 = do
  case 0b11000000 B..|. regMask0 r0 B..|. r1 of
    W8# w -> write32 t do
      B.unsafeShiftL (W32# (wordToWord32# (word8ToWord# w))) 24 B..|. 0x005f0ff2

{-# INLINE min64 #-}
-- minsd r0, r1
min64 :: ASMTape s -> Reg -> Reg -> ST s ()
min64 t r0 r1 = do
  case 0b11000000 B..|. regMask0 r0 B..|. r1 of
    W8# w -> write32 t do
      B.unsafeShiftL (W32# (wordToWord32# (word8ToWord# w))) 24 B..|. 0x005d0ff2

{-# INLINE sqrt64 #-}
-- sqrtsd r0, r1
sqrt64 :: ASMTape s -> Reg -> Reg -> ST s ()
sqrt64 t r0 r1 = do
  case 0b11000000 B..|. regMask0 r0 B..|. r1 of
    W8# w -> write32 t do
      B.unsafeShiftL (W32# (wordToWord32# (word8ToWord# w))) 24 B..|. 0x00510ff2

{-# INLINE add64' #-}
-- addsd r0, [ r1 + off ]
add64' :: ASMTape s -> Reg -> Reg -> Int -> ST s ()
add64' t r0 r1 off = do
  case 0b10000000 B..|. regMask0 r0 B..|. r1 of
    W8# w -> case 8 * off of
      I# i ->
        write64 t do
          B.unsafeShiftL (W64# (wordToWord64# (word8ToWord# w))) 24
            B..|. 0x0000000000580ff2
            B..|. B.unsafeShiftL (W64# (wordToWord64# (int2Word# i))) 32

{-# INLINE sub64' #-}
-- subsd r0, [ r1 + off ]
sub64' :: ASMTape s -> Reg -> Reg -> Int -> ST s ()
sub64' t r0 r1 off = do
  case 0b10000000 B..|. regMask0 r0 B..|. r1 of
    W8# w -> case 8 * off of
      I# i ->
        write64 t do
          B.unsafeShiftL (W64# (wordToWord64# (word8ToWord# w))) 24
            B..|. 0x00000000005c0ff2
            B..|. B.unsafeShiftL (W64# (wordToWord64# (int2Word# i))) 32

{-# INLINE mul64' #-}
-- mulsd r0, [ r1 + off ]
mul64' :: ASMTape s -> Reg -> Reg -> Int -> ST s ()
mul64' t r0 r1 off = do
  case 0b10000000 B..|. regMask0 r0 B..|. r1 of
    W8# w -> case 8 * off of
      I# i ->
        write64 t do
          B.unsafeShiftL (W64# (wordToWord64# (word8ToWord# w))) 24
            B..|. 0x0000000000590ff2
            B..|. B.unsafeShiftL (W64# (wordToWord64# (int2Word# i))) 32

{-# INLINE div64' #-}
-- divsd r0, [ r1 + off ]
div64' :: ASMTape s -> Reg -> Reg -> Int -> ST s ()
div64' t r0 r1 off = do
  case 0b10000000 B..|. regMask0 r0 B..|. r1 of
    W8# w -> case 8 * off of
      I# i ->
        write64 t do
          B.unsafeShiftL (W64# (wordToWord64# (word8ToWord# w))) 24
            B..|. 0x00000000005e0ff2
            B..|. B.unsafeShiftL (W64# (wordToWord64# (int2Word# i))) 32

{-# INLINE max64' #-}
-- maxsd r0, [ r1 + off ]
max64' :: ASMTape s -> Reg -> Reg -> Int -> ST s ()
max64' t r0 r1 off = do
  case 0b10000000 B..|. regMask0 r0 B..|. r1 of
    W8# w -> case 8 * off of
      I# i ->
        write64 t do
          B.unsafeShiftL (W64# (wordToWord64# (word8ToWord# w))) 24
            B..|. 0x00000000005f0ff2
            B..|. B.unsafeShiftL (W64# (wordToWord64# (int2Word# i))) 32

{-# INLINE min64' #-}
-- minsd r0, [ r1 + off ]
min64' :: ASMTape s -> Reg -> Reg -> Int -> ST s ()
min64' t r0 r1 off = do
  case 0b10000000 B..|. regMask0 r0 B..|. r1 of
    W8# w -> case 8 * off of
      I# i ->
        write64 t do
          B.unsafeShiftL (W64# (wordToWord64# (word8ToWord# w))) 24
            B..|. 0x00000000005d0ff2
            B..|. B.unsafeShiftL (W64# (wordToWord64# (int2Word# i))) 32

{-# INLINE copy #-}
-- movsd r0, r1
copy :: ASMTape s -> Reg -> Reg -> ST s ()
copy t r0 r1 = do
  case 0b11000000 B..|. regMask0 r0 B..|. r1 of
    W8# w -> write32 t do
          B.unsafeShiftL (W32# (wordToWord32# (word8ToWord# w))) 24
            B..|. 0x00100ff2

-- used for backpropping max partials. implemented as
-- mov rdx, o1
-- mov rcx, o2
-- mov rax, rsi
-- ucomisd r1, r0
-- cmovc rcx , rdx
-- add rax, rcx
{-# INLINE maxHelper #-}
maxHelper :: ASMTape s -> Reg -> Reg -> Int -> Int -> ST s ()
maxHelper t r0 r1 o1 o2 = do
  write16 t 0xba48
  write64 t case 8 * o1 of
    I# i -> W64# (wordToWord64# (int2Word# i))
  write16 t 0xb948
  write64 t case 8 * o2 of
    I# i -> W64# (wordToWord64# (int2Word# i))
  write32 t 0xf0894890
  case 0b11000000 B..|. regMask0 r1 B..|. r0 of
    W8# w -> write32 t do
          B.unsafeShiftL (W32# (wordToWord32# (word8ToWord# w))) 24
            B..|. 0x002e0f66
  write64 t 0x48ca420fca420f90
  write32 t 0xc8014890

-- used for backpropping min partials. implemented as
-- mov rdx, o2
-- mov rcx, o1
-- mov rax, rsi
-- ucomisd r1, r0
-- cmovc rcx , rdx
-- add rax, rcx
{-# INLINE minHelper #-}
minHelper :: ASMTape s -> Reg -> Reg -> Int -> Int -> ST s ()
minHelper t r0 r1 o2 o1 = do
  write16 t 0xba48
  write64 t case 8 * o1 of
    I# i -> W64# (wordToWord64# (int2Word# i))
  write16 t 0xb948
  write64 t case 8 * o2 of
    I# i -> W64# (wordToWord64# (int2Word# i))
  write32 t 0xf0894890
  case 0b11000000 B..|. regMask0 r1 B..|. r0 of
    W8# w -> write32 t do
          B.unsafeShiftL (W32# (wordToWord32# (word8ToWord# w))) 24
            B..|. 0x002e0f66
  write64 t 0x48ca420fca420f90
  write32 t 0xc8014890

-- used for backpropping abs partial. if r0 <= r1,
-- store 1, in r0, otherwise, store -1 in r0. implemented as
-- xor eax, eax
-- xor ecx, ecx
-- inc eax
-- dec ecx
-- ucomisd r0, r1
-- cmovc eax, ecx
-- cvtsi2sd r0, eax
{-# INLINE absHelper #-}
absHelper :: ASMTape s -> Reg -> Reg -> ST s ()
absHelper t r0 r1 = do
  -- put 1 in eax, -1 in ecx
  write64 t 0xc9ffc0ffc931c031
  -- compare and move
  case 0b11000000 B..|. regMask0 r0 B..|. r1 of
    W8# w ->
      write64 t do
        B.unsafeShiftL (W64# (wordToWord64# (word8ToWord# w))) 24
          B..|. 0x90c1420f002e0f66
  -- convert and move into destination fp register
  case 0b11000000 B..|. regMask0 r0 of
    W8# w -> 
      write32 t do
        B.unsafeShiftL (W32# (wordToWord32# (word8ToWord# w))) 24
          B..|. 0x002a0ff2       

{-# INLINE ret #-}
ret :: ASMTape s -> ST s ()
ret t = write64 t 0xc3

{-# INLINE regMask0 #-}
regMask0 :: Reg -> Word8
regMask0 w = B.unsafeShiftL w 3

