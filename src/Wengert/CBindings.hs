{-# LANGUAGE UnboxedTuples #-}
{-# LANGUAGE UnliftedFFITypes #-}

module Wengert.CBindings
  ( type ASMTape
  , allocTape
  , write8
  , write16
  , write32
  , write64
  , runTape
  , sin_addr
  , cos_addr
  , tan_addr
  , asin_addr
  , acos_addr
  , atan_addr
  , sinh_addr
  , cosh_addr
  , tanh_addr
  , asinh_addr
  , acosh_addr
  , atanh_addr
  , atan2_addr
  , exp_addr
  , log_addr
  ) where

import Control.Monad.ST
import Control.Monad.ST.Unsafe
import GHC.Exts
import GHC.IO
import GHC.Word
import qualified Data.Primitive as P
import Foreign.C.Types
import Foreign.Marshal.Alloc
import Foreign
import qualified Data.Vector.Primitive.Mutable as MPR


data ASMTAPE
data ASMTape s = ASMTape {-# UNPACK #-} !(ForeignPtr ASMTAPE)

foreign import ccall unsafe "static unistd.h getpagesize" getpagesize :: IO CInt
foreign import ccall unsafe "static stdlib.h posix_memalign" posix_memalign :: Ptr (Ptr ASMTAPE) -> CSize -> CSize -> IO CInt
foreign import ccall unsafe "static stdlib.h &free" stdfree :: FunPtr (Ptr a -> IO ())
foreign import ccall unsafe "static sys/mman.h mprotect" mprotect :: Ptr a -> CSize -> CInt -> IO CInt
foreign import ccall unsafe "static wengert.h run_tape" run_tape :: Ptr ASMTAPE -> MutableByteArray# s -> MutableByteArray# s -> IO ()

foreign import ccall unsafe "static wengert.h sin_addr" sin_addr :: IO Word64
foreign import ccall unsafe "static wengert.h cos_addr" cos_addr :: IO Word64
foreign import ccall unsafe "static wengert.h tan_addr" tan_addr :: IO Word64
foreign import ccall unsafe "static wengert.h asin_addr" asin_addr :: IO Word64
foreign import ccall unsafe "static wengert.h acos_addr" acos_addr :: IO Word64
foreign import ccall unsafe "static wengert.h atan_addr" atan_addr :: IO Word64
foreign import ccall unsafe "static wengert.h sinh_addr" sinh_addr :: IO Word64
foreign import ccall unsafe "static wengert.h cosh_addr" cosh_addr :: IO Word64
foreign import ccall unsafe "static wengert.h tanh_addr" tanh_addr :: IO Word64
foreign import ccall unsafe "static wengert.h asinh_addr" asinh_addr :: IO Word64
foreign import ccall unsafe "static wengert.h acosh_addr" acosh_addr :: IO Word64
foreign import ccall unsafe "static wengert.h atanh_addr" atanh_addr :: IO Word64
foreign import ccall unsafe "static wengert.h atan2_addr" atan2_addr :: IO Word64
foreign import ccall unsafe "static wengert.h exp_addr" exp_addr :: IO Word64
foreign import ccall unsafe "static wengert.h log_addr" log_addr :: IO Word64


{-# NOINLINE allocTape #-}
allocTape :: Int -> ST s (ASMTape s)
allocTape minSize' = unsafeIOToST $ fmap ASMTape do
  let minSize :: CSize = fromIntegral (minSize' + 8) -- use the first 8 bits as a counter
  w <- alloca $ \pp -> do
      ps <- fromIntegral <$> getpagesize
      res <- posix_memalign pp ps case divMod minSize ps of
        (_,0) -> minSize
        (d,_) -> ps * (d + 1)
      case res of
        0 -> do
          p <- peek pp
          mprotect p minSize 0x7
          IO \s -> case p of
            Ptr a -> (# writeInt64OffAddr# a 0# (intToInt64# 8#) s , () #)
          pure p
        _ -> error "allocation failure"
  newForeignPtr stdfree w

{-# INLINE withTape #-}
withTape :: ASMTape s -> (Ptr ASMTAPE -> IO a) -> ST s a
withTape (ASMTape d) f = unsafeIOToST $ withForeignPtr d f

{-# INLINE write8 #-}
write8 :: ASMTape s -> Word8 -> ST s ()
write8 t (W8# x) = withTape t \(Ptr a) -> IO \s0 -> case readInt64OffAddr# a 0# s0 of
  (# s1 , off# #) -> case writeInt64OffAddr# a 0# (intToInt64# (int64ToInt# off# +# 1#)) s1 of
    s2 -> (# writeWord8OffAddr# (plusAddr# a (int64ToInt# off#)) 0# x s2 , () #)

{-# INLINE write16 #-}
write16 :: ASMTape s -> Word16 -> ST s ()
write16 t (W16# x) = withTape t \(Ptr a) -> IO \s0 -> case readInt64OffAddr# a 0# s0 of
  (# s1 , off# #) -> case writeInt64OffAddr# a 0# (intToInt64# (int64ToInt# off# +# 2#)) s1 of
    s2 -> (# writeWord16OffAddr# (plusAddr# a (int64ToInt# off#)) 0# x s2 , () #)

{-# INLINE write32 #-}
write32 :: ASMTape s -> Word32 -> ST s ()
write32 t (W32# x) = withTape t \(Ptr a) -> IO \s0 -> case readInt64OffAddr# a 0# s0 of
  (# s1 , off# #) -> case writeInt64OffAddr# a 0# (intToInt64# (int64ToInt# off# +# 4#)) s1 of
    s2 -> (# writeWord32OffAddr# (plusAddr# a (int64ToInt# off#)) 0# x s2 , () #)

{-# INLINE write64 #-}
write64 :: ASMTape s -> Word64 -> ST s ()
write64 t (W64# x) = withTape t \(Ptr a) -> IO \s0 -> case readInt64OffAddr# a 0# s0 of
  (# s1 , off# #) -> case writeInt64OffAddr# a 0# (intToInt64# (int64ToInt# off# +# 8#)) s1 of
    s2 -> (# writeWord64OffAddr# (plusAddr# a (int64ToInt# off#)) 0# x s2 , () #)

{-# INLINE runTape #-}
runTape :: ASMTape s -> MPR.MVector s a -> MPR.MVector s a -> ST s ()
runTape w (MPR.MVector _ _ (P.MutableByteArray mba1)) (MPR.MVector _ _ (P.MutableByteArray mba2)) =
  withTape w \p -> run_tape p mba1 mba2

