{-# LANGUAGE AllowAmbiguousTypes #-}
{-# LANGUAGE DefaultSignatures #-}
{-# LANGUAGE TypeFamilies #-}
{-# LANGUAGE UnboxedTuples #-}

-- | This library is designed to enable the construction of gradient descent algorithms that share a single Wengert tape throughout the descent, avoiding the need for repeated re-construction of the tape from the objective function. This tape preservation results in a single stream of zero-allocation instructions to be executed on unboxed mutable buffers in a hot loop, which should be very effective in evaluating domain models which utilize gradient descent optimisation tactics.
--
-- In pursuit of this design goal, explicit branching is intentionally elided, resulting in a lack of @Eq@ and @Ord@ instances. Certain common functions which entail branching internally are provided as primitives, e.g., @abs'@ and @max'@. Similarly, the @primal@ primitive is replaced with the weaker @snip@ primitive (which in AD would be @auto . primal@), which enables the quenching of gradient flow through a particular partial without entailing the ability to inspect internal state in the objective function.
--
-- Note that despite being a reverse-mode automatic differentiation implementation, this library is not well-suited as a backend for machine learning libraries, but rather targeted towards differentiable domain models which are better suited to take advantage of memoisation than parallelism.

module Wengert
  ( type Var
  , grad
  , grad'
  , advanceN
  , momentumN
  , GState(..)
  , iterateg
  , auto
  , Snip(..)
  , WOrd(..)
  , atan2'
  , WengertPrim
  ) where

import Control.Monad (when)
import Control.Monad.ST
import Control.Monad.ST.Unsafe
import qualified Control.Monad.Trans.State as S
import qualified Data.Bit as B
import qualified Data.Bits as B
import Data.Coerce
import Data.Foldable
import Data.Functor.Const
import qualified Data.Vector.Generic.Mutable as MGV
import qualified Data.Vector.Mutable as MV
import Data.Vector.Packed (Pack)
import qualified Data.Vector.Packed.Mutable as MPV
import qualified Data.Vector.Primitive as PR
import qualified Data.Vector.Primitive.Mutable as MPR
import qualified Data.Vector.Unboxed.Mutable as MUV
import Foreign.C.Types (CInt(..))
import GHC.Exts
import GHC.Generics
import GHC.Int
import Math.NumberTheory.Logarithms

import qualified EShare as ES
import EShare.Hashable128
import qualified Wengert.CBindings as JITC
import Wengert.JitAmd64


data Var s a =
    Var {-# UNPACK #-} !Int
  | Konst !a
  | Plus {-# UNPACK #-} !Hash128 !(Var s a) !(Var s a)
  | Times {-# UNPACK #-} !Hash128 !(Var s a) !(Var s a)
  | Inverse !(Var s a)
  | Snip !(Var s a)
  | Abs !(Var s a)
  | Max {-# UNPACK #-} !Hash128 !(Var s a) !(Var s a)
  | Min {-# UNPACK #-} !Hash128 !(Var s a) !(Var s a)
  | Exp !(Var s a)
  | Log !(Var s a)
  | Sqrt !(Var s a)
  | Sin !(Var s a)
  | Cos !(Var s a)
  | Tan !(Var s a)
  | ASin !(Var s a)
  | ACos !(Var s a)
  | ATan !(Var s a)
  | SinH !(Var s a)
  | CosH !(Var s a)
  | TanH !(Var s a)
  | ASinH !(Var s a)
  | ACosH !(Var s a)
  | ATanH !(Var s a)
  | ATan2 {-# UNPACK #-} !Hash128 !(Var s a) !(Var s a)

-- | Lift a constant.
{-# INLINE auto #-}
auto :: a -> Var s a
auto = Konst

class Snip a where
  -- | Stop the gradient from traveling through this variable. For example, minimizing @(x - y) ^ 2@ will direct @x@ and @y@ to move toward each other, whereas minimizing @(snip x - y) ^ 2@ will only influence @y@ to move toward @x@ but not influence @x@ to move toward @y@.
  snip :: a -> a

instance Snip Double where
  {-# INLINE snip #-}
  snip = id

instance Snip Float where
  {-# INLINE snip #-}
  snip = id

instance Snip (Var s a) where
  {-# INLINE snip #-}
  snip = Snip

-- | A more restricte version of Ord, permitting min/max but not arbitrary conditional branching.
class WOrd a where
  {-# INLINE min' #-}
  min' :: a -> a -> a
  default min' :: Ord a => a -> a -> a
  min' = min
  {-# INLINE max' #-}
  max' :: a -> a -> a
  default max' :: Ord a => a -> a -> a
  max' = max

instance WOrd Double
instance WOrd Float
instance WOrd (Var s Double) where
  {-# INLINE max' #-}
  max' x y = Max (Hash128 (-8255215497073464938) 5781829498737826342 <+> hash128 x <+> hash128 y) x y
  {-# INLINE min' #-}
  min' x y = Min (Hash128 (-4131519526166408610) 5793844742924507957 <+> hash128 x <+> hash128 y) x y
instance WOrd (Var s Float) where
  {-# INLINE max' #-}
  max' x y = Max (Hash128 (-8255215497073464938) 5781829498737826342 <+> hash128 x <+> hash128 y) x y
  {-# INLINE min' #-}
  min' x y = Min (Hash128 (-4131519526166408610) 5793844742924507957 <+> hash128 x <+> hash128 y) x y


{-# INLINE atan2' #-}
-- | A domain-specific version of @atan2@. This really belongs in @Floating@, and it's not really correct to have it here with full polymorphism on @a@ (as it will implicitly use the semantics induced by the physical representation, rather than permitting the user to lift their own logical semantics), but making a separate WengertFloating class just for this one operation is also un-ergonomic.
atan2' :: WengertPrim a => Var s a -> Var s a -> Var s a
atan2' x y = ATan2 (Hash128 1179882868166510408 4553648273924158362 <+> hash128 x <+> hash128 y) x y

instance WengertPrim a => Hashable128 (Var s a) where
  {-# INLINABLE hash128 #-}
  hash128 (Var v) = hash128 v
  hash128 (Konst k) = case primRep @a of
    PDouble -> hash128 (coerce k :: Double)
    PFloat -> hash128 (coerce k :: Float)
  hash128 (Plus h _ _) = h
  hash128 (Times h _ _) = h
  hash128 (Inverse x) = Hash128 7738467873090035648 1367257029935139700 <+> hash128 x
  hash128 (Snip x) = Hash128 4767497103298493109 (-7055644481370275702) <+> hash128 x
  hash128 (Abs x) = Hash128 962955576722971718 (-8045014999246110492) <+> hash128 x
  hash128 (Max h _ _) = h
  hash128 (Min h _ _) = h
  hash128 (Exp x) = Hash128 (-2540132071881220616) (-1096567690064512892) <+> hash128 x
  hash128 (Log x) = Hash128 7920128641098229682 (-3310544442520955298) <+> hash128 x
  hash128 (Sqrt x) = Hash128 (-5968194049602207750) (-5222512053891724168) <+> hash128 x
  hash128 (Sin x) = Hash128 2641281766633489416 6482360960465746612 <+> hash128 x
  hash128 (Cos x) = Hash128 (-1466704113418161935) 7602880482593299052 <+> hash128 x
  hash128 (Tan x) = Hash128 273653566152966603 (-152734921153908600) <+> hash128 x
  hash128 (ASin x) = Hash128 7829407880450921367 857170050070699160 <+> hash128 x
  hash128 (ACos x) = Hash128 3810005278829457052 7560053368412561522 <+> hash128 x
  hash128 (ATan x) = Hash128 5040434137865143435 8051616377103007281 <+> hash128 x
  hash128 (SinH x) = Hash128 6343157876161084474 (-3465142885048885976) <+> hash128 x
  hash128 (CosH x) = Hash128 8495128175919144953 4568067425029986403 <+> hash128 x
  hash128 (TanH x) = Hash128 697300568733663231 8423893565739272132 <+> hash128 x
  hash128 (ASinH x) = Hash128 1700922055267876424 (-7829239161590491370) <+> hash128 x
  hash128 (ACosH x) = Hash128 (-297328843291866361) (-5554020984654539001) <+> hash128 x
  hash128 (ATanH x) = Hash128 (-7976609593451764554) (-5654811268249132447) <+> hash128 x
  hash128 (ATan2 h _ _) = h

-- efficient equality, if needed; but currently discrimination is entirely based on hash
-- {-# INLINE (===) #-}
-- (===) :: Eq a => Var s a -> Var s a -> Bool
-- (===) x y = case reallyUnsafePtrEquality# x y of
--   0# -> goEq x y
--   _ -> True

-- {-# INLINABLE goEq #-}
-- goEq :: Eq a => Var s a -> Var s a -> Bool
-- goEq (Var v1) (Var v2) = v1 == v2
-- goEq (Konst k1) (Konst k2) = k1 == k2
-- goEq (Plus h1 x1 x2) (Plus h2 y1 y2) = case h1 == h2 of
--   True -> goEq x1 y1 && goEq x2 y2
--   w@False -> w
-- goEq (Times h1 x1 x2) (Times h2 y1 y2) = case h1 == h2 of
--   True -> goEq x1 y1 && goEq x2 y2
--   w@False -> w
-- goEq (Inverse x) (Inverse y) = goEq x y
-- goEq (Snip x) (Snip y) = goEq x y
-- goEq (Abs x) (Abs y) = goEq x y
-- goEq (Max h1 x1 x2) (Max h2 y1 y2) = case h1 == h2 of
--   True -> goEq x1 y1 && goEq x2 y2
--   w@False -> w
-- goEq (Min h1 x1 x2) (Min h2 y1 y2) = case h1 == h2 of
--   True -> goEq x1 y1 && goEq x2 y2
--   w@False -> w
-- goEq (Exp x) (Exp y) = goEq x y
-- goEq (Log x) (Log y) = goEq x y
-- goEq (Sqrt x) (Sqrt y) = goEq x y
-- goEq (Sin x) (Sin y) = goEq x y
-- goEq (Cos x) (Cos y) = goEq x y
-- goEq (Tan x) (Tan y) = goEq x y
-- goEq (ASin x) (ASin y) = goEq x y
-- goEq (ACos x) (ACos y) = goEq x y
-- goEq (ATan x) (ATan y) = goEq x y
-- goEq (SinH x) (SinH y) = goEq x y
-- goEq (CosH x) (CosH y) = goEq x y
-- goEq (TanH x) (TanH y) = goEq x y
-- goEq (ASinH x) (ASinH y) = goEq x y
-- goEq (ACosH x) (ACosH y) = goEq x y
-- goEq (ATanH x) (ATanH y) = goEq x y
-- goEq (ATan2 h1 x1 x2) (ATan2 h2 y1 y2) = case h1 == h2 of
--   True -> goEq x1 y1 && goEq x2 y2
--   w@False -> w
-- goEq _ _ = False

-- | Note that @signum@ is undefined. Other operations should be intuitive.
instance Num (Var s Double) where
  {-# INLINE (+) #-}
  (+) x y = Plus (Hash128 (-8173023147076301004) 1049238740945386123 <+> hash128 x <+> hash128 y) x y
  {-# INLINE (*) #-}
  (*) x y = Times (Hash128 2910995742456527371 8979561561216879541 <+> hash128 x <+> hash128 y) x y
  {-# INLINE negate #-}
  negate = (*) (Konst (-1))
  {-# INLINE fromInteger #-}
  fromInteger = Konst . fromInteger

  {-# INLINE abs #-}
  abs = Abs
  signum = error "Wengert: invalid call to @signum@. Operation not supported."

instance Fractional (Var s Double) where
  {-# INLINE fromRational #-}
  fromRational = Konst . fromRational
  {-# INLINE recip #-}
  recip = Inverse

instance Floating (Var s Double) where
  {-# INLINE pi #-}
  pi = Konst pi
  {-# INLINE exp #-}
  exp = Exp
  {-# INLINE log #-}
  log = Log
  {-# INLINE sin #-}
  sin = Sin
  {-# INLINE cos #-}
  cos = Cos
  {-# INLINE tan #-}
  tan = Tan
  {-# INLINE asin #-}
  asin = ASin
  {-# INLINE acos #-}
  acos = ACos
  {-# INLINE atan #-}
  atan = ATan
  {-# INLINE sinh #-}
  sinh = SinH
  {-# INLINE cosh #-}
  cosh = CosH
  {-# INLINE tanh #-}
  tanh = TanH
  {-# INLINE asinh #-}
  asinh = ASinH
  {-# INLINE acosh #-}
  acosh = ACosH
  {-# INLINE atanh #-}
  atanh = ATanH
  {-# INLINE sqrt #-}
  sqrt = Sqrt

-- | Note that @signum@ is undefined. Other operations should be intuitive.
instance Num (Var s Float) where
  {-# INLINE (+) #-}
  (+) x y = Plus (Hash128 (-8173023147076301004) 1049238740945386123 <+> hash128 x <+> hash128 y) x y
  {-# INLINE (*) #-}
  (*) x y = Times (Hash128 2910995742456527371 8979561561216879541 <+> hash128 x <+> hash128 y) x y
  {-# INLINE negate #-}
  negate = (*) (Konst (-1))
  {-# INLINE fromInteger #-}
  fromInteger = Konst . fromInteger

  {-# INLINE abs #-}
  abs = Abs
  signum = error "Wengert: invalid call to @signum@. Operation not supported."

instance Fractional (Var s Float) where
  {-# INLINE fromRational #-}
  fromRational = Konst . fromRational
  {-# INLINE recip #-}
  recip = Inverse

instance Floating (Var s Float) where
  {-# INLINE pi #-}
  pi = Konst pi
  {-# INLINE exp #-}
  exp = Exp
  {-# INLINE log #-}
  log = Log
  {-# INLINE sin #-}
  sin = Sin
  {-# INLINE cos #-}
  cos = Cos
  {-# INLINE tan #-}
  tan = Tan
  {-# INLINE asin #-}
  asin = ASin
  {-# INLINE acos #-}
  acos = ACos
  {-# INLINE atan #-}
  atan = ATan
  {-# INLINE sinh #-}
  sinh = SinH
  {-# INLINE cosh #-}
  cosh = CosH
  {-# INLINE tanh #-}
  tanh = TanH
  {-# INLINE asinh #-}
  asinh = ASinH
  {-# INLINE acosh #-}
  acosh = ACosH
  {-# INLINE atanh #-}
  atanh = ATanH
  {-# INLINE sqrt #-}
  sqrt = Sqrt

newtype Ctr s = Ctr (MPV.MVector s Int)

{-# INLINABLE advanceN #-}
-- | Advance the given number of steps by iteratively computing the gradient and adding a portion of it to the current position to compute a new position. Returns the value of the objective function at the final position, the final position, and the gradient.
advanceN :: (Traversable t , WengertPrim a)
         => Int
         -> (forall s . t (Var s a) -> Var s a)
         -> a -- | Step size, e.g., 0.05
         -> t a
         -> (a , t a , t a)
advanceN n objf step_size start = runST do
  let !(ofv , _ , p , dp) =
        iterateg
          do objf
          do GState
              do \_ -> Ctr <$> do
                   MGV.new 1
              do \(Ctr cr) vs ps -> do
                   c <- MGV.unsafeRead cr 0
                   if c >= n
                      then pure (Just c)
                      else do
                        MGV.unsafeWrite cr 0 (1+c)
                        forM_ [ 0 .. (MGV.length vs - 1) ] \i -> do
                          v <- MGV.unsafeRead vs i
                          dv <- MGV.unsafeRead ps i
                          MGV.unsafeWrite vs i (v + step_size * dv)
                        pure Nothing
          do start
  pure (ofv , p , dp)

data MomCtr a s = MomCtr {
    momStepCtr :: MPV.MVector s Int -- 1-element counter
  , momVec :: MPR.MVector s a -- prior position vec
  } deriving (Generic)


{-# INLINABLE momentumN #-}
-- | Follow the gradient with momentum the given number of steps. Returns the value of the objective function at the final position, the final position, and the gradient.
momentumN :: forall t a . (Traversable t , WengertPrim a)
         => Int
         -> (forall s . t (Var s a) -> Var s a)
         -> a -- | a hyperparameter indicating the coefficient of momentum (e.g., 0.99)
         -> a -- | a hyperparameter indicating the coefficient of the gradient/step size (e.g., 0.02)
         -> t a
         -> (a , t a , t a)
momentumN n objf mom_coe grad_coe start = runST do
  let !(ofv , _ , p , dp) =
        iterateg
          do objf
          do GState
              do \v -> MomCtr @a
                   <$> do MGV.new 1
                   <*> do mv <- MGV.unsafeNew @_ @_ @a (MGV.length v)
                          MGV.unsafeCopy mv v
                          pure mv
              do \(MomCtr cr vv) vs ps -> do
                   c <- MGV.unsafeRead cr 0
                   if c >= n
                      then pure (Just c)
                      else do
                        MGV.unsafeWrite cr 0 (1+c)
                        forM_ [ 0 .. (MGV.length vs - 1) ] \i -> do
                          v <- MGV.unsafeRead vs i
                          prior_v <- MGV.unsafeRead vv i
                          dv <- MGV.unsafeRead ps i
                          let nv = mom_coe * prior_v + dv
                          MGV.unsafeWrite vv i nv
                          MGV.unsafeWrite vs i do v + grad_coe * nv
                        pure Nothing
          do start
  pure (ofv , p , dp)

data InternalState s a = InternalState {
    tape :: {-# UNPACK #-} !(MPV.MVector s TapeOp)
  , values :: {-# UNPACK #-} !(MPR.MVector s a)
  , partials :: {-# UNPACK #-} !(MPR.MVector s a)
  , public_values :: {-# UNPACK #-} !Int
  }

-- | GState defines loop behavior, permitting tracking of arbitrary state through the process.
--
-- @initialStatate@ accepts the starting position, and returns the value to be tracked throughout the loop.
-- @stepFunction@ accepts the value you're tracking, the current position vector, and the current gradient vector. Returning @Nothing@ will continue the loop. Returning @Just x@ will terminate the loop, yielding your @x@ along with the value of the objective function and the final position and gradient as the result of @iterateg@.
data GState w f s a = GState {
    initialState :: MPR.MVector s a -> ST s (f s)
  , stepFunction :: f s -> MPR.MVector s a -> MPR.MVector s a -> ST s (Maybe w)
  }

{-# INLINE iterateg #-}
-- | Core primitive for implementing higher-level gradient descent algorithms. The inputs are as follows:
-- 
-- Input 1: The objective function.
-- Input 2: A structure defining the initial conditions and what to happen during the loop iterations
-- Input 3: The starting position
-- Output 1: The value of the objective function at the final position.
-- Output 2: The final value returned by your specification in the provided GState
-- Output 3: The final position.
-- Output 4: The gradient at the final position.
-- 
-- Using @iterateg@ is much more efficient than writing your own loop and calling @grad@ repeatedly, as @iterateg@ computes the Wengert tape exactly once at the start and re-uses it throughout the loop, whereas if you write your own loop and call @grad@ manually, the process of generating the Wengert tape from your objective function will need to be done at every iteration of your loop, which will drastically inhibit performance.
iterateg :: (Traversable t , WengertPrim a)
         => (forall s . t (Var s a) -> Var s a)
         -> (forall s . GState w f s a)
         -> t a
         -> (a , w , t a , t a)
iterateg objf gs = \start -> runST (iterateg' objf gs start)

{-# INLINABLE iterateg' #-}
iterateg' :: forall t a w s f . (Traversable t , WengertPrim a)
          => (forall s0 . t (Var s0 a) -> Var s0 a)
          -> GState w f s a
          -> t a
          -> ST s (a , w , t a , t a)
iterateg' objf (GState prep f) start = do
  let !pvs = reifyT (fmap Var [ 0 .. ]) start
      !pvc = length start
      !obj_e = objf pvs
  dd <- ES.new (fromIntegral $ intLog2 $ (1 + pvc) * 10)
  buildDD dd obj_e
  dd_s <- ES.cardinality dd
  ass <- ES.switchModes dd (fromIntegral pvc)
  
  let !tvc = pvc + dd_s
  vv <- MPR.unsafeNew tvc
  _ <- do
    let !sv = PR.fromListN pvc $ Data.Foldable.toList start
    PR.unsafeCopy (MPR.take pvc vv) sv
  pv <- MPR.unsafeNew tvc
  tape' <- buildTape ass vv pvc obj_e
  ES.free ass
  let !is = InternalState {
               tape = tape'
             , values = vv
             , partials = pv
             , public_values = pvc
             }
  theirState <- prep (MPR.take pvc vv)
  -- asmF <- jitForwardTape tape'
  -- asmB <- jitBackwardTape tape'
  theirRes <- while do
    MGV.set pv 0
    MGV.unsafeWrite pv pvc 1
    when (pvc > 0) do
      -- JITC.runTape asmF vv pv
      -- JITC.runTape asmB vv pv
      forward is
      backward is
    f theirState (MPR.take pvc vv) (MPR.take pvc pv)
  ofv <- MGV.unsafeRead (values is) pvc
  ret_vs <- PR.take pvc <$> PR.unsafeFreeze (values is)
  ret_ps <- PR.take pvc <$> PR.unsafeFreeze (partials is)
  pure (ofv , theirRes , reifyT (PR.toList ret_vs) start , reifyT (PR.toList ret_ps) start)
  where
    reifyT rs v = flip S.evalState rs do
      traverse
        do \_ -> S.get >>= \case
            (b:bs) -> do
              S.put bs
              return b
            [] -> error "iterateg: nonsense"
        do v

{-# INLINABLE while #-}
while :: Monad m => m (Maybe w) -> m w
while f = f >>= \case
  Nothing -> while f
  Just x -> pure x

{-# INLINE grad #-}
-- | Compute the gradient at a given point.
grad :: (Traversable t , WengertPrim a) => (forall s . t (Var s a) -> Var s a) -> t a -> t a
grad objf start = snd (grad' objf start)

{-# INLINABLE grad' #-}
-- | Compute the gradient and the value of a function at a given point.
grad' :: (Traversable t , WengertPrim a) => (forall s . t (Var s a) -> Var s a) -> t a -> (a , t a)
grad' objf start = case iterateg objf (GState (\_ -> pure (Const ())) \_ _ _ -> pure (Just ())) start of
  (ofv , () , _ , gradient) -> (ofv , gradient)

{-# INLINABLE forward #-}
forward :: forall a s . WengertPrim a => InternalState s a -> ST s ()
forward is = do
  let !vs = values is
  let !t = tape is
  
  for 0 (<) (MGV.length t) \i -> do
    MGV.unsafeRead t i >>= \case
      TPlus oi xi yi -> do
        x <- MGV.unsafeRead vs xi
        y <- MGV.unsafeRead vs yi
        MGV.unsafeWrite vs oi (x + y)
      TTimes oi xi yi -> do
        x <- MGV.unsafeRead vs xi
        y <- MGV.unsafeRead vs yi
        MGV.unsafeWrite vs oi (x * y)
      TSnip oi xi -> do
        x <- MGV.unsafeRead vs xi
        MGV.unsafeWrite vs oi x
      TInverse oi xi -> do
        x <- MGV.unsafeRead vs xi
        MGV.unsafeWrite vs oi (1 / x)
      TLog oi xi -> do
        x <- MGV.unsafeRead vs xi
        MGV.unsafeWrite vs oi (log x)
      TExp oi xi -> do
        x <- MGV.unsafeRead vs xi
        MGV.unsafeWrite vs oi (exp x)
      TSqrt oi xi -> do
        x <- MGV.unsafeRead vs xi
        MGV.unsafeWrite vs oi (sqrt x)
      TAbs oi xi -> do
        x <- MGV.unsafeRead vs xi
        MGV.unsafeWrite vs oi (abs x)
      TMax oi xi yi -> do
        x <- MGV.unsafeRead vs xi
        y <- MGV.unsafeRead vs yi
        MGV.unsafeWrite vs oi (max x y)
      TMin oi xi yi -> do
        x <- MGV.unsafeRead vs xi
        y <- MGV.unsafeRead vs yi
        MGV.unsafeWrite vs oi (min x y)
      TSin oi xi -> do
        x <- MGV.unsafeRead vs xi
        MGV.unsafeWrite vs oi (sin x)
      TCos oi xi -> do
        x <- MGV.unsafeRead vs xi
        MGV.unsafeWrite vs oi (cos x)
      TTan oi xi -> do
        x <- MGV.unsafeRead vs xi
        MGV.unsafeWrite vs oi (tan x)
      TASin oi xi -> do
        x <- MGV.unsafeRead vs xi
        MGV.unsafeWrite vs oi (asin x)
      TACos oi xi -> do
        x <- MGV.unsafeRead vs xi
        MGV.unsafeWrite vs oi (acos x)
      TATan oi xi -> do
        x <- MGV.unsafeRead vs xi
        MGV.unsafeWrite vs oi (atan x)
      TSinH oi xi -> do
        x <- MGV.unsafeRead vs xi
        MGV.unsafeWrite vs oi (sinh x)
      TCosH oi xi -> do
        x <- MGV.unsafeRead vs xi
        MGV.unsafeWrite vs oi (cosh x)
      TTanH oi xi -> do
        x <- MGV.unsafeRead vs xi
        MGV.unsafeWrite vs oi (tanh x)
      TASinH oi xi -> do
        x <- MGV.unsafeRead vs xi
        MGV.unsafeWrite vs oi (asinh x)
      TACosH oi xi -> do
        x <- MGV.unsafeRead vs xi
        MGV.unsafeWrite vs oi (acosh x)
      TATanH oi xi -> do
        x <- MGV.unsafeRead vs xi
        MGV.unsafeWrite vs oi (atanh x)
      TATan2 oi yi xi -> do
        y <- MGV.unsafeRead vs yi
        x <- MGV.unsafeRead vs xi
        case primRep @a of
          PDouble -> MGV.unsafeWrite vs oi (coerce (atan2 (coerce @_ @Double y) (coerce x)))
          PFloat -> MGV.unsafeWrite vs oi (coerce (atan2 (coerce @_ @Float y) (coerce x)))

{-# INLINABLE backward #-}
backward :: forall s a . WengertPrim a => InternalState s a -> ST s ()
backward is = do
  let !vs = values is
  let !t = tape is

  for' (MGV.length t - 1) (>=) 0 \i -> do
    MGV.unsafeRead t i >>= \case
      TPlus oi xi yi -> do
        d_o <- MGV.unsafeRead ps oi
        backprop d_o xi 1
        backprop d_o yi 1
      TTimes oi xi yi -> do
        d_o <- MGV.unsafeRead ps oi
        x <- MGV.unsafeRead vs xi
        y <- MGV.unsafeRead vs yi
        backprop d_o xi y
        backprop d_o yi x
      TSnip _ _ -> pure ()
      TInverse oi xi -> do
        d_o <- MGV.unsafeRead ps oi
        x <- MGV.unsafeRead vs xi
        let !x' = 1 / x
        backprop d_o xi ((-1) * x' * x')
      TAbs oi xi -> do
        d_o <- MGV.unsafeRead ps oi
        x <- MGV.unsafeRead vs xi
        backprop d_o xi if x < 0 then -1 else 1
      TMax oi xi yi -> do
        d_o <- MGV.unsafeRead ps oi
        x <- MGV.unsafeRead vs xi
        y <- MGV.unsafeRead vs yi
        if x < y
           then backprop d_o yi 1
           else backprop d_o xi 1
      TMin oi xi yi -> do
        d_o <- MGV.unsafeRead ps oi
        x <- MGV.unsafeRead vs xi
        y <- MGV.unsafeRead vs yi
        if x <= y
           then backprop d_o xi 1
           else backprop d_o yi 1
      TExp oi xi -> do
        d_o <- MGV.unsafeRead ps oi
        x <- MGV.unsafeRead vs xi
        backprop d_o xi (exp x)
      TSqrt oi xi -> do
        d_o <- MGV.unsafeRead ps oi
        x <- MGV.unsafeRead vs xi
        backprop d_o xi (1/(2*sqrt(x)))
      TLog oi xi -> do
        d_o <- MGV.unsafeRead ps oi
        x <- MGV.unsafeRead vs xi
        backprop d_o xi (1/x)
      TSin oi xi -> do
        d_o <- MGV.unsafeRead ps oi
        x <- MGV.unsafeRead vs xi
        backprop d_o xi (cos x)
      TCos oi xi -> do
        d_o <- MGV.unsafeRead ps oi
        x <- MGV.unsafeRead vs xi
        backprop d_o xi (-1 * sin x)
      TTan oi xi -> do
        d_o <- MGV.unsafeRead ps oi
        x <- MGV.unsafeRead vs xi
        backprop d_o xi (2 / (cos (2 * x) + 1))
      TASin oi xi -> do
        d_o <- MGV.unsafeRead ps oi
        x <- MGV.unsafeRead vs xi
        backprop d_o xi (1 / sqrt (1 - x * x))
      TACos oi xi -> do
        d_o <- MGV.unsafeRead ps oi
        x <- MGV.unsafeRead vs xi
        backprop d_o xi ((-1) / sqrt (1 - x * x))
      TATan oi xi -> do
        d_o <- MGV.unsafeRead ps oi
        x <- MGV.unsafeRead vs xi
        backprop d_o xi (1 / (x * x + 1))
      TSinH oi xi -> do
        d_o <- MGV.unsafeRead ps oi
        x <- MGV.unsafeRead vs xi
        backprop d_o xi (cosh x)
      TCosH oi xi -> do
        d_o <- MGV.unsafeRead ps oi
        x <- MGV.unsafeRead vs xi
        backprop d_o xi (sinh x)
      TTanH oi xi -> do
        d_o <- MGV.unsafeRead ps oi
        x <- MGV.unsafeRead vs xi
        backprop d_o xi (2 / (cosh (2 * x) + 1))
      TASinH oi xi -> do
        d_o <- MGV.unsafeRead ps oi
        x <- MGV.unsafeRead vs xi
        backprop d_o xi (1 / sqrt (x * x + 1))
      TACosH oi xi -> do
        d_o <- MGV.unsafeRead ps oi
        x <- MGV.unsafeRead vs xi
        backprop d_o xi (1 / (sqrt (x * x - 1)))
      TATanH oi xi -> do
        d_o <- MGV.unsafeRead ps oi
        x <- MGV.unsafeRead vs xi
        backprop d_o xi (1 / (1 - x * x))
      TATan2 oi yi xi -> do
        d_o <- MGV.unsafeRead ps oi
        y <- MGV.unsafeRead vs yi
        x <- MGV.unsafeRead vs xi
        let !z = x * x + y * y
        backprop d_o xi ((-1 * y) / z)
        backprop d_o yi (x / z)

  where
    !ps = partials is
    
    {-# INLINE backprop #-}
    backprop :: a -> Int -> a -> ST s ()
    backprop d_o i x = do
      MGV.unsafeRead ps i >>= \v -> MGV.unsafeWrite ps i (v + d_o * x)

{-# INLINABLE buildDD #-}
buildDD :: WengertPrim a => ES.DeDuper s -> Var s0 a -> ST s ()
buildDD r = \v -> go v
  where
    go (Var _) = pure ()
    go v@(Konst _) = ES.insert r (hash128 v) >> pure ()
    go (Plus h x y) = do
      ES.insert r h >>= \case
        0 -> pure ()
        _ -> go x >> go y
    go (Times h x y) = do
      ES.insert r h >>= \case
        0 -> pure ()
        _ -> go x >> go y
    go v@(Inverse x) = ES.insert r (hash128 v) >>= \case
      0 -> pure ()
      _ -> go x
    go v@(Snip x) = ES.insert r (hash128 v) >>= \case
      0 -> pure ()
      _ -> go x
    go v@(Abs x) = ES.insert r (hash128 v) >>= \case
      0 -> pure ()
      _ -> go x
    go (Max h x y) = do
      ES.insert r h >>= \case
        0 -> pure ()
        _ -> go x >> go y
    go (Min h x y) = do
      ES.insert r h >>= \case
        0 -> pure ()
        _ -> go x >> go y
    go v@(Sqrt x) = ES.insert r (hash128 v) >>= \case
      0 -> pure ()
      _ -> go x
    go v@(Log x) = ES.insert r (hash128 v) >>= \case
      0 -> pure ()
      _ -> go x
    go v@(Exp x) = ES.insert r (hash128 v) >>= \case
      0 -> pure ()
      _ -> go x
    go v@(Sin x) = ES.insert r (hash128 v) >>= \case
      0 -> pure ()
      _ -> go x
    go v@(Cos x) = ES.insert r (hash128 v) >>= \case
      0 -> pure ()
      _ -> go x
    go v@(Tan x) = ES.insert r (hash128 v) >>= \case
      0 -> pure ()
      _ -> go x
    go v@(ASin x) = ES.insert r (hash128 v) >>= \case
      0 -> pure ()
      _ -> go x
    go v@(ACos x) = ES.insert r (hash128 v) >>= \case
      0 -> pure ()
      _ -> go x
    go v@(ATan x) = ES.insert r (hash128 v) >>= \case
      0 -> pure ()
      _ -> go x
    go v@(SinH x) = ES.insert r (hash128 v) >>= \case
      0 -> pure ()
      _ -> go x
    go v@(CosH x) = ES.insert r (hash128 v) >>= \case
      0 -> pure ()
      _ -> go x
    go v@(TanH x) = ES.insert r (hash128 v) >>= \case
      0 -> pure ()
      _ -> go x
    go v@(ASinH x) = ES.insert r (hash128 v) >>= \case
      0 -> pure ()
      _ -> go x
    go v@(ACosH x) = ES.insert r (hash128 v) >>= \case
      0 -> pure ()
      _ -> go x
    go v@(ATanH x) = ES.insert r (hash128 v) >>= \case
      0 -> pure ()
      _ -> go x
    go (ATan2 h x y) = do
      ES.insert r h >>= \case
        0 -> pure ()
        _ -> go x >> go y

-- binary representation of tape op
data TapeOp = TapeOp {-# UNPACK #-} !Int32 -- op code
                     {-# UNPACK #-} !Int32 -- output
                     {-# UNPACK #-} !Int32 -- input 1
                     {-# UNPACK #-} !Int32 -- input 2 (if applicable)
  deriving (Generic,Show,Pack)

pattern PlusOpCode :: Int32
pattern PlusOpCode <- 0
  where
    PlusOpCode = 0

pattern TimesOpCode :: Int32
pattern TimesOpCode <- 1
  where
    TimesOpCode = 1

pattern InverseOpCode :: Int32
pattern InverseOpCode <- 2
  where
    InverseOpCode = 2

pattern SnipOpCode :: Int32
pattern SnipOpCode <- 3
  where
    SnipOpCode = 3

pattern AbsOpCode :: Int32
pattern AbsOpCode <- 4
  where
    AbsOpCode = 4

pattern ExpOpCode :: Int32
pattern ExpOpCode <- 5
  where
    ExpOpCode = 5

pattern SqrtOpCode :: Int32
pattern SqrtOpCode <- 6
  where
    SqrtOpCode = 6

pattern SinOpCode :: Int32
pattern SinOpCode <- 7
  where
    SinOpCode = 7

pattern CosOpCode :: Int32
pattern CosOpCode <- 8
  where
    CosOpCode = 8

pattern TanOpCode :: Int32
pattern TanOpCode <- 9
  where
    TanOpCode = 9

pattern ASinOpCode :: Int32
pattern ASinOpCode <- 10
  where
    ASinOpCode = 10

pattern ACosOpCode :: Int32
pattern ACosOpCode <- 11
  where
    ACosOpCode = 11

pattern ATanOpCode :: Int32
pattern ATanOpCode <- 12
  where
    ATanOpCode = 12

pattern SinHOpCode :: Int32
pattern SinHOpCode <- 13
  where
    SinHOpCode = 13

pattern CosHOpCode :: Int32
pattern CosHOpCode <- 14
  where
    CosHOpCode = 14

pattern TanHOpCode :: Int32
pattern TanHOpCode <- 15
  where
    TanHOpCode = 15

pattern ASinHOpCode :: Int32
pattern ASinHOpCode <- 16
  where
    ASinHOpCode = 16

pattern ACosHOpCode :: Int32
pattern ACosHOpCode <- 17
  where
    ACosHOpCode = 17

pattern ATanHOpCode :: Int32
pattern ATanHOpCode <- 18
  where
    ATanHOpCode = 18

pattern LogOpCode :: Int32
pattern LogOpCode <- 19
  where
    LogOpCode = 19

pattern MaxOpCode :: Int32
pattern MaxOpCode <- 20
  where
    MaxOpCode = 20

pattern MinOpCode :: Int32
pattern MinOpCode <- 21
  where
    MinOpCode = 21

pattern ATan2OpCode :: Int32
pattern ATan2OpCode <- 22
  where
    ATan2OpCode = 22

{-# INLINE i2i32 #-}
i2i32 :: Int -> Int32
i2i32 (I# i) = I32# (intToInt32# i)

{-# INLINE i322i #-}
i322i :: Int32 -> Int
i322i (I32# i) = I# (int32ToInt# i)

pattern TPlus :: Int -> Int -> Int -> TapeOp
pattern TPlus o x y <- TapeOp PlusOpCode (i322i -> o) (i322i -> x) (i322i -> y)
  where
    TPlus o x y = TapeOp PlusOpCode (i2i32 o) (i2i32 x) (i2i32 y)

pattern TTimes :: Int -> Int -> Int -> TapeOp
pattern TTimes o x y <- TapeOp TimesOpCode (i322i -> o) (i322i -> x) (i322i -> y)
  where
    TTimes o x y = TapeOp TimesOpCode (i2i32 o) (i2i32 x) (i2i32 y)

pattern TInverse :: Int -> Int -> TapeOp
pattern TInverse o x <- TapeOp InverseOpCode (i322i -> o) (i322i -> x) _
  where
    TInverse o x = TapeOp InverseOpCode (i2i32 o) (i2i32 x) 0

pattern TSnip :: Int -> Int -> TapeOp
pattern TSnip o x <- TapeOp SnipOpCode (i322i -> o) (i322i -> x) _
  where
    TSnip o x = TapeOp SnipOpCode (i2i32 o) (i2i32 x) 0

pattern TAbs :: Int -> Int -> TapeOp
pattern TAbs o x <- TapeOp AbsOpCode (i322i -> o) (i322i -> x) _
  where
    TAbs o x = TapeOp AbsOpCode (i2i32 o) (i2i32 x) 0

pattern TMax :: Int -> Int -> Int -> TapeOp
pattern TMax o x y <- TapeOp MaxOpCode (i322i -> o) (i322i -> x) (i322i -> y)
  where
    TMax o x y = TapeOp MaxOpCode (i2i32 o) (i2i32 x) (i2i32 y)

pattern TMin :: Int -> Int -> Int -> TapeOp
pattern TMin o x y <- TapeOp MinOpCode (i322i -> o) (i322i -> x) (i322i -> y)
  where
    TMin o x y = TapeOp MinOpCode (i2i32 o) (i2i32 x) (i2i32 y)

pattern TExp :: Int -> Int -> TapeOp
pattern TExp o x <- TapeOp ExpOpCode (i322i -> o) (i322i -> x) _
  where
    TExp o x = TapeOp ExpOpCode (i2i32 o) (i2i32 x) 0

pattern TLog :: Int -> Int -> TapeOp
pattern TLog o x <- TapeOp LogOpCode (i322i -> o) (i322i -> x) _
  where
    TLog o x = TapeOp LogOpCode (i2i32 o) (i2i32 x) 0

pattern TSqrt :: Int -> Int -> TapeOp
pattern TSqrt o x <- TapeOp SqrtOpCode (i322i -> o) (i322i -> x) _
  where
    TSqrt o x = TapeOp SqrtOpCode (i2i32 o) (i2i32 x) 0

pattern TSin :: Int -> Int -> TapeOp
pattern TSin o x <- TapeOp SinOpCode (i322i -> o) (i322i -> x) _
  where
    TSin o x = TapeOp SinOpCode (i2i32 o) (i2i32 x) 0

pattern TCos :: Int -> Int -> TapeOp
pattern TCos o x <- TapeOp CosOpCode (i322i -> o) (i322i -> x) _
  where
    TCos o x = TapeOp CosOpCode (i2i32 o) (i2i32 x) 0

pattern TTan :: Int -> Int -> TapeOp
pattern TTan o x <- TapeOp TanOpCode (i322i -> o) (i322i -> x) _
  where
    TTan o x = TapeOp TanOpCode (i2i32 o) (i2i32 x) 0

pattern TASin :: Int -> Int -> TapeOp
pattern TASin o x <- TapeOp ASinOpCode (i322i -> o) (i322i -> x) _
  where
    TASin o x = TapeOp ASinOpCode (i2i32 o) (i2i32 x) 0

pattern TACos :: Int -> Int -> TapeOp
pattern TACos o x <- TapeOp ACosOpCode (i322i -> o) (i322i -> x) _
  where
    TACos o x = TapeOp ACosOpCode (i2i32 o) (i2i32 x) 0

pattern TATan :: Int -> Int -> TapeOp
pattern TATan o x <- TapeOp ATanOpCode (i322i -> o) (i322i -> x) _
  where
    TATan o x = TapeOp ATanOpCode (i2i32 o) (i2i32 x) 0

pattern TSinH :: Int -> Int -> TapeOp
pattern TSinH o x <- TapeOp SinHOpCode (i322i -> o) (i322i -> x) _
  where
    TSinH o x = TapeOp SinHOpCode (i2i32 o) (i2i32 x) 0

pattern TCosH :: Int -> Int -> TapeOp
pattern TCosH o x <- TapeOp CosHOpCode (i322i -> o) (i322i -> x) _
  where
    TCosH o x = TapeOp CosHOpCode (i2i32 o) (i2i32 x) 0

pattern TTanH :: Int -> Int -> TapeOp
pattern TTanH o x <- TapeOp TanHOpCode (i322i -> o) (i322i -> x) _
  where
    TTanH o x = TapeOp TanHOpCode (i2i32 o) (i2i32 x) 0

pattern TASinH :: Int -> Int -> TapeOp
pattern TASinH o x <- TapeOp ASinHOpCode (i322i -> o) (i322i -> x) _
  where
    TASinH o x = TapeOp ASinHOpCode (i2i32 o) (i2i32 x) 0

pattern TACosH :: Int -> Int -> TapeOp
pattern TACosH o x <- TapeOp ACosHOpCode (i322i -> o) (i322i -> x) _
  where
    TACosH o x = TapeOp ACosHOpCode (i2i32 o) (i2i32 x) 0

pattern TATanH :: Int -> Int -> TapeOp
pattern TATanH o x <- TapeOp ATanHOpCode (i322i -> o) (i322i -> x) _
  where
    TATanH o x = TapeOp ATanHOpCode (i2i32 o) (i2i32 x) 0

pattern TATan2 :: Int -> Int -> Int -> TapeOp
pattern TATan2 o x y <- TapeOp ATan2OpCode (i322i -> o) (i322i -> x) (i322i -> y)
  where
    TATan2 o x y = TapeOp ATan2OpCode (i2i32 o) (i2i32 x) (i2i32 y)



{-# COMPLETE TPlus , TTimes , TInverse , TSnip , TAbs , TMax , TMin , TLog , TSqrt , TExp , TSin , TCos , TTan , TASin , TACos , TATan , TSinH , TCosH , TTanH , TASinH , TACosH , TATanH , TATan2 #-}

{-# INLINABLE buildTape #-}
buildTape :: forall tv vv s s0 a . (MGV.MVector tv TapeOp , MGV.MVector vv a , WengertPrim a)
          => ES.Assigner s -> vv s a -> Int -> Var s0 a -> ST s (tv s TapeOp)
buildTape ass vs pvc e = do

  let !tvc = MGV.length vs

  (deps , pt , ks) <- do
    -- dependency graph via mapping an expression to the expressions which depend on it
    deps :: MV.MVector s [ Int ] <- MGV.unsafeNew tvc
    MGV.set deps []

    -- track which vars are constants so they can be used together with public variables
    -- as the start of the computation graph
    kv :: MV.MVector s [ Int ] <- MGV.unsafeNew 1
    MGV.unsafeWrite kv 0 []

    -- pre-tape buffer, tracking the operations used to compute each expression corresponding
    -- to each index. these expessions are not in the final order (hence, pre-tape), which
    -- will be subsequently computed via the dependency graph.
    pt :: tv s TapeOp <- MGV.unsafeNew tvc

    -- vs is here to write known information like Konstants
    -- into the value buffer while doing the expression traversal.
    -- 
    -- pt tracks without redundancy the operations which will go on our tape.
    buildGraph vs deps pt kv ass e
    
    pure (deps , pt , kv)

  -- now, trace the dependency graph to order the tape properly
  seen :: MUV.MVector s B.Bit <- MGV.new tvc

  -- initialize with given vars and constants
  MGV.set (MGV.take pvc seen) 1
  konstants <- MGV.unsafeRead ks 0
  for_ konstants \i -> MGV.unsafeWrite seen i 1

  -- re-purpose this ks to act as a queue
  let queue = ks

  -- add public vars to the queue
  MGV.unsafeRead queue 0 >>= \is -> MGV.unsafeWrite queue 0 ([0 .. pvc - 1] <> is)

  -- allocate the final tape
  t <- MGV.unsafeNew (tvc - pvc - length konstants)

  -- counter for tracking current position while building tape
  ctr <- MPR.unsafeNew 1
  MPR.unsafeWrite ctr 0 (0 :: Int)
  
  pretapeToTape seen deps pt queue ctr t
  
  pure t

{-# INLINABLE pretapeToTape #-}
-- topologically sort the pretape to get a valid tape that can be traversed forwards and backwards.
-- 
-- potential TODO: write snip in as 0 to the partial value vector, then constant fold (forward
-- and backward) to generate optimized forward and backward tapes with the redundant ops elided.
-- for short walks, this may or may not even be an improvement, but for long walks it should
-- perform better.
pretapeToTape :: forall sv dv qv cv tv s .
                 ( MGV.MVector sv B.Bit
                 , MGV.MVector dv [ Int ]
                 , MGV.MVector qv [ Int ]
                 , MGV.MVector cv Int
                 , MGV.MVector tv TapeOp
                 )
              => sv s B.Bit -> dv s [ Int ] -> tv s TapeOp -> qv s [ Int ] -> cv s Int -> tv s TapeOp
              -> ST s ()
pretapeToTape seen deps pt queue ctr t = go
  where
    go :: ST s ()
    go = MGV.unsafeRead queue 0 >>= \case
      [] -> pure ()
      (x:xs) -> do
        MGV.unsafeWrite queue 0 xs
        MGV.unsafeRead deps x >>= traverse_ \i -> do
          already_done <- MGV.unsafeRead seen i
          when (not (coerce already_done)) do
            MGV.unsafeRead pt i >>= \case
              op@(TapeOp _ _ xi32 yi32) -> do
                let xi = i322i xi32
                let yi = i322i yi32
                bx <- MGV.unsafeRead seen xi
                by <- MGV.unsafeRead seen yi
                when (coerce bx && coerce by) do
                  c <- MGV.unsafeRead ctr 0
                  MGV.unsafeWrite ctr 0 (c + 1)
                  MGV.unsafeWrite t c op
                  MGV.unsafeWrite seen i 1
                  MGV.unsafeRead queue 0 >>= \is -> MGV.unsafeWrite queue 0 (i:is)
        go

{-# INLINABLE buildGraph #-}
-- traverse through the AST to set everything up for topological sorting
buildGraph :: forall pt vv kv dv s s0 a .
              ( MGV.MVector pt TapeOp
              , MGV.MVector vv a
              , MGV.MVector dv [ Int ]
              , MGV.MVector kv [ Int ]
              , WengertPrim a
              )
           => vv s a -> dv s [ Int ] -> pt s TapeOp -> kv s [ Int ] -> ES.Assigner s -> Var s0 a
           -> ST s ()
buildGraph vs deps pt kv ass = \e -> do
  go e
  pure ()
  where
    {-# INLINABLE go #-}
    go (Var i) = pure i
    go v@(Konst k) = assignKnown v >>= \i -> if i < 0 then pure (-1 * i) else do
      MGV.unsafeWrite vs i k
      MGV.unsafeRead kv 0 >>= \kis -> MGV.unsafeWrite kv 0 (i:kis)
      pure i
    go v@(Plus _ x y) = assignKnown v >>= \i -> if i < 0 then pure (-1 * i) else do
      xi <- go x
      yi <- go y
      MGV.unsafeWrite pt i (TPlus i xi yi)
      MGV.unsafeRead deps xi >>= \is -> MGV.unsafeWrite deps xi (i:is)
      MGV.unsafeRead deps yi >>= \is -> MGV.unsafeWrite deps yi (i:is)
      pure i
    go v@(Times _ x y) = assignKnown v >>= \i -> if i < 0 then pure (-1 * i) else do
      xi <- go x
      yi <- go y
      MGV.unsafeWrite pt i (TTimes i xi yi)
      MGV.unsafeRead deps xi >>= \is -> MGV.unsafeWrite deps xi (i:is)
      MGV.unsafeRead deps yi >>= \is -> MGV.unsafeWrite deps yi (i:is)
      pure i
    go v@(Inverse x) = assignKnown v >>= \i -> if i < 0 then pure (-1 * i) else do
      xi <- go x
      MGV.unsafeWrite pt i (TInverse i xi)
      MGV.unsafeRead deps xi >>= \is -> MGV.unsafeWrite deps xi (i:is)
      pure i
    go v@(Snip x) = assignKnown v >>= \i -> if i < 0 then pure (-1 * i) else do
      xi <- go x
      MGV.unsafeWrite pt i (TSnip i xi)
      MGV.unsafeRead deps xi >>= \is -> MGV.unsafeWrite deps xi (i:is)
      pure i
    go v@(Abs x) = assignKnown v >>= \i -> if i < 0 then pure (-1 * i) else do
      xi <- go x
      MGV.unsafeWrite pt i (TAbs i xi)
      MGV.unsafeRead deps xi >>= \is -> MGV.unsafeWrite deps xi (i:is)
      pure i
    go v@(Max _ x y) = assignKnown v >>= \i -> if i < 0 then pure (-1 * i) else do
      xi <- go x
      yi <- go y
      MGV.unsafeWrite pt i (TMax i xi yi)
      MGV.unsafeRead deps xi >>= \is -> MGV.unsafeWrite deps xi (i:is)
      MGV.unsafeRead deps yi >>= \is -> MGV.unsafeWrite deps yi (i:is)
      pure i
    go v@(Min _ x y) = assignKnown v >>= \i -> if i < 0 then pure (-1 * i) else do
      xi <- go x
      yi <- go y
      MGV.unsafeWrite pt i (TMin i xi yi)
      MGV.unsafeRead deps xi >>= \is -> MGV.unsafeWrite deps xi (i:is)
      MGV.unsafeRead deps yi >>= \is -> MGV.unsafeWrite deps yi (i:is)
      pure i
    go v@(Exp x) = assignKnown v >>= \i -> if i < 0 then pure (-1 * i) else do
      xi <- go x
      MGV.unsafeWrite pt i (TExp i xi)
      MGV.unsafeRead deps xi >>= \is -> MGV.unsafeWrite deps xi (i:is)
      pure i
    go v@(Log x) = assignKnown v >>= \i -> if i < 0 then pure (-1 * i) else do
      xi <- go x
      MGV.unsafeWrite pt i (TLog i xi)
      MGV.unsafeRead deps xi >>= \is -> MGV.unsafeWrite deps xi (i:is)
      pure i
    go v@(Sqrt x) = assignKnown v >>= \i -> if i < 0 then pure (-1 * i) else do
      xi <- go x
      MGV.unsafeWrite pt i (TSqrt i xi)
      MGV.unsafeRead deps xi >>= \is -> MGV.unsafeWrite deps xi (i:is)
      pure i
    go v@(Sin x) = assignKnown v >>= \i -> if i < 0 then pure (-1 * i) else do
      xi <- go x
      MGV.unsafeWrite pt i (TSin i xi)
      MGV.unsafeRead deps xi >>= \is -> MGV.unsafeWrite deps xi (i:is)
      pure i
    go v@(Cos x) = assignKnown v >>= \i -> if i < 0 then pure (-1 * i) else do
      xi <- go x
      MGV.unsafeWrite pt i (TCos i xi)
      MGV.unsafeRead deps xi >>= \is -> MGV.unsafeWrite deps xi (i:is)
      pure i
    go v@(Tan x) = assignKnown v >>= \i -> if i < 0 then pure (-1 * i) else do
      xi <- go x
      MGV.unsafeWrite pt i (TTan i xi)
      MGV.unsafeRead deps xi >>= \is -> MGV.unsafeWrite deps xi (i:is)
      pure i
    go v@(ASin x) = assignKnown v >>= \i -> if i < 0 then pure (-1 * i) else do
      xi <- go x
      MGV.unsafeWrite pt i (TASin i xi)
      MGV.unsafeRead deps xi >>= \is -> MGV.unsafeWrite deps xi (i:is)
      pure i
    go v@(ACos x) = assignKnown v >>= \i -> if i < 0 then pure (-1 * i) else do
      xi <- go x
      MGV.unsafeWrite pt i (TACos i xi)
      MGV.unsafeRead deps xi >>= \is -> MGV.unsafeWrite deps xi (i:is)
      pure i
    go v@(ATan x) = assignKnown v >>= \i -> if i < 0 then pure (-1 * i) else do
      xi <- go x
      MGV.unsafeWrite pt i (TATan i xi)
      MGV.unsafeRead deps xi >>= \is -> MGV.unsafeWrite deps xi (i:is)
      pure i
    go v@(SinH x) = assignKnown v >>= \i -> if i < 0 then pure (-1 * i) else do
      xi <- go x
      MGV.unsafeWrite pt i (TSinH i xi)
      MGV.unsafeRead deps xi >>= \is -> MGV.unsafeWrite deps xi (i:is)
      pure i
    go v@(CosH x) = assignKnown v >>= \i -> if i < 0 then pure (-1 * i) else do
      xi <- go x
      MGV.unsafeWrite pt i (TCosH i xi)
      MGV.unsafeRead deps xi >>= \is -> MGV.unsafeWrite deps xi (i:is)
      pure i
    go v@(TanH x) = assignKnown v >>= \i -> if i < 0 then pure (-1 * i) else do
      xi <- go x
      MGV.unsafeWrite pt i (TTanH i xi)
      MGV.unsafeRead deps xi >>= \is -> MGV.unsafeWrite deps xi (i:is)
      pure i
    go v@(ASinH x) = assignKnown v >>= \i -> if i < 0 then pure (-1 * i) else do
      xi <- go x
      MGV.unsafeWrite pt i (TASinH i xi)
      MGV.unsafeRead deps xi >>= \is -> MGV.unsafeWrite deps xi (i:is)
      pure i
    go v@(ACosH x) = assignKnown v >>= \i -> if i < 0 then pure (-1 * i) else do
      xi <- go x
      MGV.unsafeWrite pt i (TACosH i xi)
      MGV.unsafeRead deps xi >>= \is -> MGV.unsafeWrite deps xi (i:is)
      pure i
    go v@(ATanH x) = assignKnown v >>= \i -> if i < 0 then pure (-1 * i) else do
      xi <- go x
      MGV.unsafeWrite pt i (TATanH i xi)
      MGV.unsafeRead deps xi >>= \is -> MGV.unsafeWrite deps xi (i:is)
      pure i
    go v@(ATan2 _ x y) = assignKnown v >>= \i -> if i < 0 then pure (-1 * i) else do
      xi <- go x
      yi <- go y
      MGV.unsafeWrite pt i (TATan2 i xi yi)
      MGV.unsafeRead deps xi >>= \is -> MGV.unsafeWrite deps xi (i:is)
      MGV.unsafeRead deps yi >>= \is -> MGV.unsafeWrite deps yi (i:is)
      pure i

    {-# INLINE assignKnown #-}
    assignKnown :: Var s0 a -> ST s Int
    assignKnown (hash128 -> h) = ES.assignKnown ass h >>= \case
      CInt (I32# i_) -> pure (I# (int32ToInt# i_))

data PRep a where
  PDouble :: PRep Double
  PFloat :: PRep Float

-- | The primitives available for use.
class (Floating a , MPR.Prim a , Ord a , Coercible a (WPrimRep a)) => WengertPrim a where
  type WPrimRep a
  primRep :: PRep (WPrimRep a)

instance WengertPrim Double where
  type WPrimRep Double = Double
  {-# INLINE primRep #-}
  primRep = PDouble
instance WengertPrim Float where
  type WPrimRep Float = Float
  {-# INLINE primRep #-}
  primRep = PFloat

{-# INLINE for #-}
-- non-allocating iterator
for :: Applicative f => Int -> (Int -> Int -> Bool) -> Int -> (Int -> f ()) -> f ()
for s b e f = loop s
  where
    loop i = if b i e
      then f i *> loop (i + 1)
      else pure ()

{-# INLINE for' #-}
-- non-allocating iterator
for' :: Applicative f => Int -> (Int -> Int -> Bool) -> Int -> (Int -> f ()) -> f ()
for' s b e f = loop s
  where
    loop i = if b i e
      then f i *> loop (i - 1)
      else pure ()


-----------------------------------------------
-- Lightweight Amd64 JIT for tape generation --
-----------------------------------------------

jitForwardTape :: forall s . MPV.MVector s TapeOp -> ST s (JITC.ASMTape s)
jitForwardTape ops = do

  sin_a <- unsafeIOToST JITC.sin_addr
  cos_a <- unsafeIOToST JITC.cos_addr
  tan_a <- unsafeIOToST JITC.tan_addr
  asin_a <- unsafeIOToST JITC.asin_addr
  acos_a <- unsafeIOToST JITC.acos_addr
  atan_a <- unsafeIOToST JITC.atan_addr
  sinh_a <- unsafeIOToST JITC.sinh_addr
  cosh_a <- unsafeIOToST JITC.cosh_addr
  tanh_a <- unsafeIOToST JITC.tanh_addr
  asinh_a <- unsafeIOToST JITC.asinh_addr
  acosh_a <- unsafeIOToST JITC.acosh_addr
  atanh_a <- unsafeIOToST JITC.atanh_addr
  atan2_a <- unsafeIOToST JITC.atan2_addr
  exp_a <- unsafeIOToST JITC.exp_addr
  log_a <- unsafeIOToST JITC.log_addr
  
  -- cycle through registers to permit as much pipelining as possible
  ireg <- MPR.new 1
  let getR :: ST s Reg
      getR = do
        r <- MGV.unsafeRead ireg 0
        MGV.unsafeWrite ireg 0 (r + 1)
        pure (0b00000111 B..&. r)
        
  let !vs = arg0
  
  t <- JITC.allocTape (1 + MGV.length ops * 5 * 8) -- worst is 5 ops, each used op is 8 bytes max
  JITC.write32 t 0x08ec8348 -- sub rbp 8 to properly align stack pointer
  
  for 0 (<) (MGV.length ops) \idx -> MGV.unsafeRead ops idx >>= \case
    TPlus oi xi yi -> do
      r <- getR
      load64 t r vs xi
      add64' t r vs yi
      store64 t r vs oi
    TTimes oi xi yi -> do
      r <- getR
      load64 t r vs xi
      mul64' t r vs yi
      store64 t r vs oi
    TSnip oi xi -> do
      r <- getR
      load64 t r vs xi
      store64 t r vs oi
    TInverse oi xi -> do
      r0 <- getR
      r1 <- getR
      load1 t r0
      load64 t r1 vs xi
      div64 t r0 r1
      store64 t r0 vs oi
    TLog oi xi -> do
      load64 t 0 vs xi
      call t log_a
      store64 t 0 vs oi
    TExp oi xi -> do
      load64 t 0 vs xi
      call t exp_a
      store64 t 0 vs oi
    TSqrt oi xi -> do
      r <- getR
      load64 t r vs xi
      sqrt64 t r r
      store64 t r vs oi
    TAbs oi xi -> do
      r0 <- getR
      r1 <- getR
      load64 t r1 vs xi
      loadNeg1 t r0
      mul64 t r0 r1
      max64 t r0 r1
      store64 t r0 vs oi
    TMax oi xi yi -> do
      r <- getR
      load64 t r vs xi
      max64' t r vs yi
      store64 t r vs oi
    TMin oi xi yi -> do
      r <- getR
      load64 t r vs xi
      min64' t r vs yi
      store64 t r vs oi
    TSin oi xi -> do
      load64 t 0 vs xi
      call t sin_a
      store64 t 0 vs oi
    TCos oi xi -> do
      load64 t 0 vs xi
      call t cos_a
      store64 t 0 vs oi
    TTan oi xi -> do
      load64 t 0 vs xi
      call t tan_a
      store64 t 0 vs oi
    TASin oi xi -> do
      load64 t 0 vs xi
      call t asin_a
      store64 t 0 vs oi
    TACos oi xi -> do
      load64 t 0 vs xi
      call t acos_a
      store64 t 0 vs oi
    TATan oi xi -> do
      load64 t 0 vs xi
      call t atan_a
      store64 t 0 vs oi
    TSinH oi xi -> do
      load64 t 0 vs xi
      call t sinh_a
      store64 t 0 vs oi
    TCosH oi xi -> do
      load64 t 0 vs xi
      call t cosh_a
      store64 t 0 vs oi
    TTanH oi xi -> do
      load64 t 0 vs xi
      call t tanh_a
      store64 t 0 vs oi
    TASinH oi xi -> do
      load64 t 0 vs xi
      call t asinh_a
      store64 t 0 vs oi
    TACosH oi xi -> do
      load64 t 0 vs xi
      call t acosh_a
      store64 t 0 vs oi
    TATanH oi xi -> do
      load64 t 0 vs xi
      call t atanh_a
      store64 t 0 vs oi
    TATan2 oi yi xi -> do
      load64 t 0 vs yi
      load64 t 1 vs xi
      call t atan2_a
      store64 t 0 vs oi

  JITC.write32 t 0x08c48348 -- add epb 8 to put the stack pointer back

  ret t
  
  pure t


jitBackwardTape :: forall s . MPV.MVector s TapeOp -> ST s (JITC.ASMTape s)
jitBackwardTape ops = do
  
  sin_a <- unsafeIOToST JITC.sin_addr
  cos_a <- unsafeIOToST JITC.cos_addr
  sinh_a <- unsafeIOToST JITC.sinh_addr
  cosh_a <- unsafeIOToST JITC.cosh_addr
  exp_a <- unsafeIOToST JITC.exp_addr

  -- cycle through registers to permit as much pipelining as possible
  ireg <- MPR.new 1
  let getR :: ST s Reg
      !getR = do
        r <- MGV.unsafeRead ireg 0
        MGV.unsafeWrite ireg 0 (r + 1)
        pure (0b00000111 B..&. r)

  let !vs = arg0
  let !ps = arg1
  
  t <- JITC.allocTape (1 + MGV.length ops * 15 * 8) -- worst is 17 ops
  JITC.write32 t 0x08ec8348 -- sub rbp 8 to properly align stack pointer

  for' (MGV.length ops - 1) (>=) 0 \idx -> MGV.unsafeRead ops idx >>= \case
    TPlus oi xi yi -> do
      r0 <- getR
      r1 <- getR
      load64 t r0 ps oi
      copy t r1 r0
      add64' t r0 ps xi
      add64' t r1 ps yi
      store64 t r0 ps xi
      store64 t r1 ps yi
    TTimes oi xi yi -> do
      r0 <- getR
      r1 <- getR
      load64 t r0 ps oi
      copy t r1 r0
      mul64' t r0 vs yi
      mul64' t r1 vs xi
      add64' t r0 ps xi
      add64' t r1 ps yi
      store64 t r0 ps xi
      store64 t r1 ps yi
    TSnip _ _ -> pure ()
    TInverse oi xi -> do
      r0 <- getR
      r1 <- getR
      load1 t r0
      loadNeg1 t r1
      div64' t r0 vs xi
      mul64' t r1 ps oi
      mul64 t r0 r0
      mul64 t r0 r1
      add64' t r0 ps xi
      store64 t r0 ps xi
    TAbs oi xi -> do
      r0 <- getR
      r1 <- getR
      load64 t r0 vs xi
      loadNeg1 t r1
      mul64 t r1 r0
      absHelper t r0 r1
      mul64' t r0 ps oi
      add64' t r0 ps xi
      store64 t r0 ps xi
    TMax oi xi yi -> do
      r0 <- getR
      r1 <- getR
      r2 <- getR
      load64 t r0 vs xi
      load64 t r1 vs yi
      load64 t r2 ps oi
      maxHelper t r0 r1 xi yi
      add64' t r2 resultReg 0
      store64 t r2 resultReg 0
    TMin oi xi yi -> do
      r0 <- getR
      r1 <- getR
      r2 <- getR
      load64 t r0 vs xi
      load64 t r1 vs yi
      load64 t r2 ps oi
      minHelper t r0 r1 xi yi
      add64' t r2 resultReg 0
      store64 t r2 resultReg 0
    TExp oi xi -> do
      load64 t 0 vs xi
      call t exp_a
      mul64' t 0 ps oi
      add64' t 0 ps xi
      store64 t 0 ps xi
    TSqrt oi xi -> do
      r0 <- getR
      r1 <- getR
      r2 <- getR
      load1 t r0
      load64 t r1 vs xi
      copy t r0 r2
      add64 t r2 r0
      sqrt64 t r1 r1
      mul64 t r1 r2
      div64 t r0 r1
      mul64' t r0 ps oi
      add64' t r0 ps xi
      store64 t r0 ps xi
    TLog oi xi -> do
      r0 <- getR
      load1 t r0
      div64' t r0 vs xi
      mul64' t r0 ps oi
      add64' t r0 ps xi
      store64 t r0 ps xi
    TSin oi xi -> do
      load64 t 0 vs xi
      call t cos_a
      mul64' t 0 ps oi
      add64' t 0 ps xi
      store64 t 0 ps xi
    TCos oi xi -> do
      load64 t 0 vs xi
      call t sin_a
      loadNeg1 t 1
      mul64' t 1 ps oi
      mul64 t 1 0
      add64' t 1 ps xi
      store64 t 1 ps xi
    TTan oi xi -> do
      load64 t 0 vs xi
      load1 t 1
      add64 t 1 1
      mul64 t 0 1
      call t cos_a
      load1 t 1
      copy t 2 1
      add64 t 1 1
      add64 t 2 0
      div64 t 1 2
      mul64' t 1 ps oi
      add64' t 1 ps xi
      store64 t 1 ps xi
    TASin oi xi -> do
      r0 <- getR
      r1 <- getR
      r2 <- getR
      load64 t r0 vs xi
      load1 t r1
      copy t r2 r1
      mul64 t r0 r0
      sub64 t r1 r0
      sqrt64 t r1 r1
      div64 t r2 r1
      mul64' t r2 ps oi
      add64' t r2 ps xi
      store64 t r2 ps xi
    TACos oi xi -> do
      r0 <- getR
      r1 <- getR
      r2 <- getR
      load64 t r0 vs xi
      load1 t r1
      loadNeg1 t r2
      mul64 t r0 r0
      sub64 t r1 r0
      sqrt64 t r1 r1
      div64 t r2 r1
      mul64' t r2 ps oi
      add64' t r2 ps xi
      store64 t r2 ps xi
    TATan oi xi -> do
      r0 <- getR
      r1 <- getR
      load64 t r0 vs xi
      load1 t r1
      mul64 t r0 r0
      add64 t r0 r1
      div64 t r1 r0
      mul64' t r1 ps oi
      add64' t r1 ps xi
      store64 t r1 ps xi
    TSinH oi xi -> do
      load64 t 0 vs xi
      call t cosh_a
      mul64' t 0 ps oi
      add64' t 0 ps xi
      store64 t 0 ps xi
    TCosH oi xi -> do
      load64 t 0 vs xi
      call t sinh_a
      mul64' t 0 ps oi
      add64' t 0 ps xi
      store64 t 0 ps xi
    TTanH oi xi -> do
      load64 t 0 vs xi
      load1 t 1
      add64 t 1 1
      mul64 t 0 1
      call t cosh_a
      load1 t 1
      copy t 2 1
      add64 t 1 1
      add64 t 2 0
      div64 t 1 2
      mul64' t 1 ps oi
      add64' t 1 ps xi
      store64 t 1 ps xi
    TASinH oi xi -> do
      r0 <- getR
      r1 <- getR
      r2 <- getR
      load64 t r0 vs xi
      load1 t r1
      copy t r2 r1
      mul64 t r0 r0
      add64 t r1 r0
      sqrt64 t r1 r1
      div64 t r2 r1
      mul64' t r2 ps oi
      add64' t r2 ps xi
      store64 t r2 ps xi
    TACosH oi xi -> do
      r0 <- getR
      r1 <- getR
      r2 <- getR
      load64 t r0 vs xi
      load1 t r1
      copy t r2 r1
      mul64 t r0 r0
      sub64 t r0 r1
      sqrt64 t r0 r0
      div64 t r2 r0
      mul64' t r2 ps oi
      add64' t r2 ps xi
      store64 t r2 ps xi
    TATanH oi xi -> do
      r0 <- getR
      r1 <- getR
      r2 <- getR
      load64 t r0 vs xi
      load1 t r1
      copy t r2 r1
      mul64 t r0 r0
      sub64 t r2 r0
      div64 t r1 r2
      mul64' t r1 ps oi
      add64' t r1 ps xi
      store64 t r1 ps xi
    TATan2 oi yi xi -> do
      r0 <- getR
      r1 <- getR
      r2 <- getR
      r3 <- getR
      load64 t r0 vs yi
      load64 t r1 vs xi
      loadNeg1 t r2
      copy t r3 r1
      mul64 t r2 r0
      mul64 t r0 r0
      mul64 t r1 r1
      add64 t r0 r1
      load64 t r1 ps oi
      div64 t r2 r0
      div64 t r3 r0
      mul64 t r2 r1
      mul64 t r3 r1
      add64' t r2 ps xi
      add64' t r3 ps yi
      store64 t r2 ps xi
      store64 t r3 ps yi

  JITC.write32 t 0x08c48348 -- add epb 8 to put the stack pointer back
  ret t
  
  pure t

